module gitlab.com/suthisa.k/firestoremint

go 1.13

require (
	cloud.google.com/go/firestore v1.5.0
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.3.0
	github.com/mitchellh/mapstructure v1.4.1
	github.com/myesui/uuid v1.0.0 // indirect
	github.com/twinj/uuid v1.0.0
	google.golang.org/api v0.48.0
	gopkg.in/stretchr/testify.v1 v1.2.2 // indirect
)

package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/twinj/uuid"

	"cloud.google.com/go/firestore"
	"github.com/gorilla/handlers"
	// firebase "firebase.google.com/go"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"github.com/mitchellh/mapstructure"
	"google.golang.org/api/iterator"
	//"google.golang.org/api/option"
)

type App struct {
	Router *mux.Router
	client *firestore.Client
	ctx    context.Context
}

// var projectID = os.Getenv("GOOGLE_CLOUD_PROJECT")

func main() {

	godotenv.Load()
	route := App{}
	route.Init()
	route.Run()
}

func setupResponse(w *http.ResponseWriter, req *http.Request) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
    (*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
    (*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
}

func (route *App) Init() {

	projectID := "hotel-beta-314304"

	route.ctx = context.Background()

	var err error
	route.client, err = firestore.NewClient(route.ctx, projectID)
	if err != nil {
		log.Fatalf("Firestore: %v", err)
	}

	route.Router = mux.NewRouter()
	route.initializeRoutes()
	fmt.Println("Successfully connected at port : " + route.GetPort())
}

func (route *App) GetPort() string {
	var port = os.Getenv("MyPort")
	port = "8000"
	return ":" + port
}

func (route *App) Run() {
	log.Fatal(http.ListenAndServe(route.GetPort(), handlers.CORS(handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"}), handlers.AllowedMethods([]string{"GET", "POST", "PUT", "HEAD", "OPTIONS"}), handlers.AllowedOrigins([]string{"*"}))(route.Router)))
	//log.Fatal(http.ListenAndServe(route.GetPort(), route.Router))
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"error": message})
}

func (route *App) initializeRoutes() {

	route.Router.HandleFunc("/users", route.GetUsers).Methods("GET")
	route.Router.HandleFunc("/user/{id}", route.GetUser).Methods("GET")
	route.Router.HandleFunc("/user", route.CreateUser).Methods("POST")
	route.Router.HandleFunc("/user/{id}", route.UpdateUser).Methods("PUT")
	route.Router.HandleFunc("/user/{id}", route.DeleteUser).Methods("DELETE")
	route.Router.HandleFunc("/user-login", route.Login).Methods("POST")
}

func (route *App) GetUsers(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w, r)
	var usersData []Users

	iter := route.client.Collection("login").Documents(route.ctx)
	for {
		var user Users
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Fatalf("Failed to iterate: %v", err)
		}

		mapstructure.Decode(doc.Data(), &user)
		usersData = append(usersData, user)
	}
	respondWithJSON(w, http.StatusOK, usersData)
}

func (route *App) GetUser(w http.ResponseWriter, r *http.Request) {

	setupResponse(&w, r)

	params := mux.Vars(r)
	paramsID := params["id"]
	var usersData []Users

	iter := route.client.Collection("login").Where("User_id", "==", paramsID).Documents(route.ctx)
	for {
		var user Users
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Fatalf("Failed to iterate: %v", err)
		}

		mapstructure.Decode(doc.Data(), &user)
		usersData = append(usersData, user)
	}

	respondWithJSON(w, http.StatusOK, usersData)
}

func (route *App) CreateUser(w http.ResponseWriter, r *http.Request) {

	setupResponse(&w, r)

	uid := uuid.NewV4()
	splitID := strings.Split(uid.String(), "-")
	id := splitID[0] + splitID[1] + splitID[2] + splitID[3] + splitID[4]

	var usersData Users

	Decoder := json.NewDecoder(r.Body)
	err := Decoder.Decode(&usersData)
	usersData.User_id = id

	if err != nil {
		respondWithJSON(w, http.StatusInternalServerError, "Error when create user")
	}else{

		var checkEmail []Users
		iter := route.client.Collection("login").Where("Email", "==", usersData.Email).Documents(route.ctx)
		for {
			var user Users
			doc, err := iter.Next()
			if err == iterator.Done {
				break
			}
			if err != nil {
				log.Fatalf("Failed to iterate: %v", err)
			}

			mapstructure.Decode(doc.Data(), &user)
			checkEmail = append(checkEmail, user)
		}

		if checkEmail != nil{
			respondWithJSON(w, http.StatusInternalServerError, "Error when create user")
		}else{
			_, _, err = route.client.Collection("login").Add(route.ctx, usersData)
			if err != nil {
				log.Printf("An error has occurred: %s", err)
			}
			fmt.Println(usersData)
			respondWithJSON(w, http.StatusCreated, "Create user success!")
		}
	}
}

func (route *App) UpdateUser(w http.ResponseWriter, r *http.Request) {

	setupResponse(&w, r)

	params := mux.Vars(r)
	paramsID := params["id"]

	var usersData Users

	Decoder := json.NewDecoder(r.Body)
	err := Decoder.Decode(&usersData)
	if err != nil {
		log.Printf("error: %s", err)
	}

	var docID string
	usersData.User_id = paramsID

	iter := route.client.Collection("login").Where("User_id", "==", paramsID).Documents(route.ctx)
	for {
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Fatalf("Failed to iterate: %v", err)
		}
		docID = doc.Ref.ID
	}

	if docID != ""{
		_, err = route.client.Collection("login").Doc(docID).Set(route.ctx, usersData)
		if err != nil {
			log.Printf("An error has occurred: %s", err)
		}else{
			fmt.Println(usersData)
			fmt.Println(docID)
			respondWithJSON(w, http.StatusCreated, "Edit user success!")
		}
	}
}

func (route *App) DeleteUser(w http.ResponseWriter, r *http.Request) {

	setupResponse(&w, r)

	params := mux.Vars(r)
	paramsID := params["id"]

	var docID string

	iter := route.client.Collection("login").Where("User_id", "==", paramsID).Documents(route.ctx)
	for {
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Fatalf("Failed to iterate: %v", err)
		}
		docID = doc.Ref.ID
	}

	_, err := route.client.Collection("login").Doc(docID).Delete(route.ctx)
	if err != nil {
		log.Printf("An error has occurred: %s", err)
	}

	respondWithJSON(w, http.StatusOK, "Delete user success !")
}

func (route *App) Login(w http.ResponseWriter, r *http.Request) {

	setupResponse(&w, r)

	var userLogin Users
	Decoder := json.NewDecoder(r.Body)
	err := Decoder.Decode(&userLogin)
	if err != nil {
		respondWithJSON(w, http.StatusInternalServerError, "Error when get user")
	}else{

		if userLogin.Email != ""{
			var usersData Users
			checkEmail := route.client.Collection("login").Where("Email", "==", userLogin.Email).Documents(route.ctx)
			for {
				var user Users
			
				doc, err := checkEmail.Next()
				if err == iterator.Done {
					break
				}
				if err != nil {
					log.Printf("error: %s", err)
				}
	
				mapstructure.Decode(doc.Data(), &user)
				usersData = user
			}
			if userLogin.Password != "" && usersData.Password == userLogin.Password{
				respondWithJSON(w, http.StatusOK, "Login success")
			}else{
				respondWithJSON(w, http.StatusInternalServerError, "Error when login")
			}
		}else{
			respondWithJSON(w, http.StatusInternalServerError, "Error when get user")
		}
		

	}



}
 
